# OUTPUT SCREENSHOTS

## Output in console

![Console Output](./screenshots/Output1.jpg?raw=true, "Console Output") 

## Output in workbench

![Workbench Output](./screenshots/Output2.jpg?raw=true, "Workbench Output") 