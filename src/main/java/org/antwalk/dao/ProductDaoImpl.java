package org.antwalk.dao;

import java.util.List;

import javax.sql.DataSource;

import org.antwalk.entity.Product;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public class ProductDaoImpl implements ProductDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplateObject(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setDataSource(DataSource ds) {
		this.dataSource = ds;// initializing connection
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void insert(Product product) {

		try {
			String SQL = "INSERT INTO Product (productId, productName) VALUES (?, ?)";
			jdbcTemplate.update(SQL, product.getProductId(), product.getProductName());
			System.out.println(
					"Inserted Product = (" + product.getProductId() + ",\"" + product.getProductName() + "\")");
			// to simulate the exception.
			// throw new RuntimeException("simulate Error condition") ;
		} catch (DataAccessException e) {
			System.out.println("Error in creating record, rolling back");
			throw e;
		}

	}

	public Product getProduct(Integer productId) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Product> listProducts() {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(Integer productId) {
		// TODO Auto-generated method stub

	}

	public void update(Integer productId, String productName) {
		// TODO Auto-generated method stub

	}

}
