package org.antwalk.dao;

import java.util.List;

import javax.sql.DataSource;

import org.antwalk.entity.Product;

public interface ProductDao {

	void setDataSource(DataSource ds);

	void insert(Product product);

	Product getProduct(Integer productId);

	List<Product> listProducts();

	void delete(Integer productId);

	void update(Integer productId, String productName);
}
