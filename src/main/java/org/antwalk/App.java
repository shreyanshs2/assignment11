package org.antwalk;

import org.antwalk.dao.ProductDao;
import org.antwalk.entity.Product;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");

		ProductDao productDao = applicationContext.getBean("ProductDaoImpl", ProductDao.class);

		productDao.insert(new Product(25, "pro25"));
	}
}